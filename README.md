# Dida Service Bus

A lightweight PHP Service Bus library. MIT License.

## API

```php
public static function has($name);
public static function set($name, $service);
public static function setSingleton($name, $service);
public static function get($name, array $parameters = []);
public static function getNew($name, array $parameters = []);
public static function remove($name);
public static function names();
```

## Examples

### set

```php
// set a classname as service
ServiceBus::set('Request', \Dida\Http\Request::class);

// Set an instance as service.
ServiceBus::set("App", $app);

// Set a closure as service
ServiceBus::set("Db", function () use ($foo, $bar) {
    $conf = require __DIR__ . "/conf/mysql.php";
    $conf["foo"] = $foo;
    $conf["bar"] = $bar;
    $db = new \Dida\Db\Db($conf);
    return $db;
});
```
